<?php
class Question {
    public $number;
    public $question;
    public $answers;
    private $correct_answer;

    public function __construct($number, $question, $answers, $correct_answer){
        $this->number = $number;
        $this->question = $question;
        $this->answers = $answers;
        $this->correct_answer = $correct_answer;
    }

    public function get_correct(){
        return $this->correct_answer;
    }
}

$questions_page_1 = array(
    new Question(1, "không có ..... tồn tại thuần túy không chưa đựng ..... ngược lại cũng không có .... lại không tồn tại trong một .... xác định?", array("hình thức / nội dung / nội dung / hình thức", "nội dung / hình thức / hình thức / nội dung", "hiện tượng / bản chất / bản chất / hiện tượng", "bản chất / hiện tượng / hiện tượng / bản chất"), 1),
    new Question(2, "mối quan hệ biện chứng giữa nội dung và hình thức?", array("song song, đối lập nhau", "phụ thuộc tuyệt đối vào nhau", "gắn bó chặt chẽ, không tách rời nhau", "tách rời riêng biệt nhau"), 3),
    new Question(3, "giữa nội dung và hình thức yếu tố nào biến đổi chậm hơn?", array("nội dung", "hình thức", "cả nội dung và hình thức", "không có phương án đúng"), 2),
    new Question(4, "trong quá trình vận động phát triển của sự vật ..... giữ vai trò quyết định .....?", array("hình thức - nội dung", "nội dung - hình thức", "ý thức - vật chất", "hiện tượng - bản chất"), 2),
    new Question(5, "trong các cụm từ dưới đây, cụm từ nào được xem là 'hình thức' trong cặp phạm trù 'nội dung - hình thức' mà phép biện chứng duy vật nghiên cứu 'Truyện Kiều là .....'?", array("tác phẩm của Nguyễn Du", "tác phẩm thơ lục bát", "tác phẩm có bìa màu xanh", "tác phẩm ra đời vào thế kỉ XVIII"), 2),
);


$questions_page_2 = array(
    new Question(6, "Hệ thống triết học nào quan niệm triết học là “yêu mến sự thông thái”?", array("Triết học Trung Quốc Cổ đại", "Triết học Ấn Độ cổ đại", "Triết học Hy Lạp cổ đại", "Triết học cổ điển Đức"), 3),
    new Question(7, "Hệ thống triết học nào quan niệm triết học là “chiêm ngưỡng”, hàm ý là tri thức dựa trên lý trí, là con đường suy nghĩ để dẫn dắt con người đến với lẽ phải?", array("Triết học Trung Quốc Cổ đại", "Triết học Ấn Độ cổ đại", "Triết học Hy Lạp cổ đại", "Triết học cổ điển Đức"), 2),
    new Question(8, "Hệ thống triết học nào quan niệm: “ Triết học là hệ thống quan điểm lý luận chung nhất về thế giới và vị trí con con người trong thế giới đó, là khoa học về những quy luật vận động, phát triển    chung nhất của tự nhiên, xã hội và tư duy”?", array("Triết học Trung Quốc Cổ đại", "Triết học Ấn Độ cổ đại", "Triết học Marx - Lenin", "Triết học cổ điển Đức"), 3),
    new Question(9, "Điền thuật ngữ chính xác vào chỗ trống: “Triết học là hệ thống quan điểm ……… về thế giới và vị trí con người trong thế giới đó, là khoa học về những quy luật vận động, phát triển chung nhất của tự nhiên, xã hội và tư duy”", array("Lý luận", "Lý luận chung nhất", "Thực tiễn", "Kinh nghiệm"), 2),
    new Question(10, "Triết học bao gồm quan điểm chung nhất, những sự lý giải có luận chứng cho các câu hỏi chung của con người nên triết học bao gồm toàn bộ tri thức của nhân loại. Kết luận trên ứng với triết học thời kỳ nào ?", array("Triết học cổ đại", "Triết học Phục Hưng", "Triết học Trung cổ Tây Âu", "Triết học Marx - Lenin"), 1),
);

$all_correct_answers = array();
foreach(array_merge($questions_page_1, $questions_page_2) as $question){
    $all_correct_answers["ques_".$question->number] = $question->get_correct();
}
?>
